# Syme

Syme is a project to add access control to [Newspeak](http://newspeaklanguage.org/).

Also, [Syme is a character](https://en.wikipedia.org/wiki/Syme_%281984%29#Secondary_characters) in [Orwell's famous 1984](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four) who works at the [Ministry of Truth](https://bitbucket.org/newspeaklanguage/). Syme specializes in language. He is working on a new edition of the Newspeak dictionary.

The modifications will (or will not) be merged into newspeak some day. This repository becomes obsolete once this is done.

## What has been done

* two new bytecodes were introduced
  `245 ExtSelfSendBytecode` and `246 ExtOuterSendBytecode`
* the newspeak compiler was changed to emit those bytecodes
* the VM can differentiate between the different newspeak sends (normal send, public send, self send, super send, outer send, implicit reciever send) and alters the super-chain lookup accordingly
* a warning is printed to stdout on access violations (no enforcement yet)

## Want to give it a try?

There is a newspeak VM in `binaries/nsvmlinux/nsvm` (compiled on Ubuntu 13.04), open the test-image with:

    binaries/nsvmlinux/nsvm images/nsboot-2013-06.image

and run the tests. You should see that the VM print a lot of warnings when it thinks there is an access violation. (e.g. a private method was called from a public send)

The output for a single access violation looks like this:

    Not accessible method (NoMNU) # Brazil`AreaClasses`Area`999>visual

Here the `visual` message has been sent to a `Brazil'AreaClasses'Area` object.
